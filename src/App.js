import React, { Component } from 'react';
//import Printer, { print } from 'react-pdf-print';
import ReactDOM from "react-dom";
import './css/App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Collapse from 'react-bootstrap/Collapse';
import * as d3 from "d3";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
//import colorpicker from "vanderlee-colorpicker";
//import jsXML from "node-jsxml";
import * as $ from "jquery";
import saveAs from 'file-saver';

//testing commit again
const SudokuGenerator = require("js-sudoku-generator").SudokuGenerator;
const color ="red";
const DOMParser = require("xmldom").DOMParser;
const XMLSerializer = require("xmldom").XMLSerializer;

function hello(val){
    alert("hi " + val)
    console.log(val)
}
var diff = "Sudoku lett"; var xml1, xml2, xml3, xml4, xml5, xml6, xml7 = '';

function generateMyBoard(nr, diff_level, isPrint, resNr){
    console.log(resNr + "  resNr --------------")
    var nr2 =  1;
  var isPrint = isPrint;
  if(resNr){nr2=resNr}
  console.log ("nr: "+nr+"diff_level: "+diff_level+" isprint : "+isPrint+" resNr: "+resNr)
  SudokuGenerator.generate(3);

  // get difficulty sheets
  let l_oFirstBoard = SudokuGenerator.generatedBoards[0];

  // get sheet signature (for loading)
 // console.log(l_oFirstBoard.signature);

  // load saved board 
  let l_oLoadedBoard = SudokuGenerator.loadBoard(l_oFirstBoard.signature);

  // get hard difficulty sheet
  let l_aHardSheet = l_oFirstBoard.getSheet(2);

  // get medium difficulty sheet
  let l_aMediumSheet = l_oFirstBoard.getSheet(1);
  
  // get easy difficulty sheet
  let l_aEasySheet = l_oFirstBoard.getSheet(0);
  
  // pretty print solution to console
  //l_oFirstBoard.prettyPrint();
  
  //var testing = $("#root").html(l_oFirstBoard)
  // pretty print sheet
  //l_oFirstBoard.prettyPrint(l_aEasySheet);
  var mySudoku;
  var headline = "";
  if(diff_level == 0){
      mySudoku = l_aEasySheet;
      headline = diff1;
      diff = diff1;
  }else if(diff_level == 1){
    mySudoku = l_aMediumSheet;
    headline = diff2;
    diff = diff2;
  }else {
    mySudoku = l_aHardSheet;
    headline = diff3;
    diff = diff3;
  }
  var nr = nr;
 
  var Grid = gridData(l_oFirstBoard, mySudoku, nr2);
 // console.log(Grid)
  var node = <div></div>;
  //$("#myResults"+nr2+" .grid"+nr+" .svgC").html("");sol1
  $("#myResults"+nr2+" .grid"+nr+" .hl").html( headline ).attr("style","color:"+hlcolor);
  $("#myResults"+nr2+" .sol"+nr+" .hl").html( headline ).attr("style","color:"+hlcolor+"; font-size:1rem;margin-left:193px;margin-bottom:-23px;");//<h6 style='color:"+hlcolor+";text-align:right;margin-right:253px;'>
  $("#myResults"+nr2+" .sol"+nr+" .svgC").html("");
 
  $("#myResults"+nr2+" .grid"+nr+" .svgC").html("");
  var grid = d3.select("#myResults"+nr2+" .grid"+nr+" .svgC")
    .append("svg")
    .attr("width","510px")
    .attr("height","510px");

    var row = grid.selectAll(".row")
    .data(Grid)
    .enter().append("g")
    .attr("class", "row")
    .style("stroke-width",1);  

    var column = row.selectAll(".square")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("class","square")
    .attr("x", function(d) { return d.x + 10; })
    .attr("y", function(d) { return d.y + 10; })
    .attr("width", function(d) { return d.width; })
    .attr("height", function(d) { return d.height; })
    .style("fill", "#fff")
    .style("stroke-width", 1)
    .style("stroke", thincolor)
    .style("fill",function(d) { return d.bgColor; });
    
    var text = row.selectAll(".text")
      .data(function(d){return d;})
      .enter().append("text")
      .attr("x", function(d) { return d.x +26; })
      .attr("y", function(d) { return d.y + 44; })
      .attr("width", function(d) { return d.width; })
      .attr("height", function(d) { return d.height; })
      .style("font-size","28px")
      .style("font-family", "arial")
      .style("fill", fontcolor)
      .text(function(d){return d.value;})


      for(var i = 0; i < 9 / 3; i++) {
        for(var j = 0; j < 9 / 3; j++) {
            grid.append("rect")
                .attr("height", 450/3)
                .attr("width", 450/3)
                .attr("transform", "translate("+ (i * 450/3   +10) + "," + (j * 450/3  +10) + ")")
                .attr("fill-opacity", "0")
                .attr("pointer-events", "none")
                .classed("box", true)
                .style("stroke-width", 4)
                
                .style("stroke",thickcolor);
        }
      }
      

      //LOESUNG hl-result
     // $("#myResults"+nr2+" .sol"+nr+"").html("<h6 style='color:"+hlcolor+";text-align:right;margin-right:253px;'>Løsning</h6>").attr("style","")
      $("#myResults"+nr2+" .hl-result").html("<h6 style='color:"+hlcolor+";text-align:left;margin-left:200px;'>Løsning</h6>").attr("style","")
    var grid2 = d3.select("#myResults"+nr2+" .sol"+nr +" .svgC")
    .append("svg")
    .attr("width","510px")
    .attr("height","510px")
    .attr("viewBox", "-85 -400 900 900");

    var row2 = grid2.selectAll(".row")
    .data(Grid)
    .enter().append("g")
    .attr("class", "row")
    .style("stroke-width",1);  

    var column2 = row2.selectAll(".square")
    .data(function(d) { return d; })
    .enter().append("rect")
    .attr("class","square")
    .attr("x", function(d) { return d.x + 10; })
    .attr("y", function(d) { return d.y + 10; })
    .attr("width", function(d) { return d.width; })
    .attr("height", function(d) { return d.height; })
    .style("fill", "#fff")

    .style("stroke-width", 1)
    .style("stroke", "#222")
    .style("fill",function(d) { return d.bgColor; });
 
    
    var text2 = row2.selectAll(".text")
      .data(function(d){return d;})
      .enter().append("text")
      .attr("x", function(d) { return d.x +26; })
      .attr("y", function(d) { return d.y + 44; })
      .attr("width", function(d) { return d.width; })
      .attr("height", function(d) { return d.height; })
      .style("font-size","28px")
      .style("font-family", "arial")
      .style("fill", fontcolor)
      .text(function(d){return d.value2;})


      for(var i = 0; i < 9 / 3; i++) {
        for(var j = 0; j < 9 / 3; j++) {
            grid2.append("rect")
                .attr("height", 450/3)
                .attr("width", 450/3)
                .attr("transform", "translate("+ (i * 450/3   +10) + "," + (j * 450/3  +10) + ")")
                .attr("fill-opacity", "0")
                .attr("pointer-events", "none")
                .classed("box", true)
                .style("stroke-width", 4)
                .style("stroke", "#222");
        }
      }
      grid2
        .attr("transform"," rotate(180 0 0)")
        
   var blub = nr2 +1;
    for(var i =blub; i <= 7;i++){
        $("#myResults"+i+"  .hl").html( "" );
        $("#myResults"+i+" .hl-result").html("");
    }
    return grid;
}
var today=new Date();
var dd = String(today. getDate()). padStart(2, '0');
var mm = String(today. getMonth() + 1). padStart(2, '0');
var yyyy = today. getFullYear();
today = dd + '_' + mm + '_' + yyyy;
var fontcolor = "#000002";
var bgcolor = "#fff";
var bgcolor2 = "#fff";
var thickcolor = "#000002";
var thincolor = "#000002";
var hlcolor = "#000002";
var theme = "none";
var diff1 = "lett Sudoku";
var diff2 = "middels Sudoku";
var diff3 = "vanskelig Sudoku";
var sReps = 1;
function handleSubmit(){
  //console.log("handlesubmit")
  $(".hl").html("");
  $(".svgC").html("");
  $(".hl-result").html("");

  //setOpen(!open);
  const input = document.getElementById('myForm');
  var form = $('#myForm2').serialize();
  var form2 =$('#myForm2').serializeArray();
  var counter = 1;
  $.each(form2, function(k,v){
    console.log(v.name+" :"+v.value)
    if(v.name == "sudokusReps"){
        if (v.value > 1){
            sReps = v.value;
        }else {
            sReps = v.value;
        }

    }
    if( v.name == "fontcolor" || 
        v.name == "bgcolor" || 
        v.name == "bgcolor2" ||
        v.name == "thickcolor" ||
        v.name == "thincolor" ||
        v.name == "hlcolor"){
            var tempstr = v.value;
            if(tempstr.charAt(0) != '#'){
                v.value = '#'+v.value;
            }

    }

    if (v.name == "fontcolor") fontcolor = v.value;
    if (v.name == "theme") theme = v.value;
    if (v.name == "bgcolor") bgcolor = v.value;
    if (v.name == "bgcolor2") bgcolor2 = v.value;
    if (v.name == "thickcolor") thickcolor = v.value;
    if (v.name == "thincolor") thincolor = v.value;
    if (v.name == "hlcolor") hlcolor = v.value;
    if (v.name == "diff1") diff1 = v.value;
    if (v.name == "diff2") diff2 = v.value;
    if (v.name == "diff3") diff3 = v.value;
    if (v.name == "diff_level") {
        if(sReps > 1){
            makePdf(sReps, form2)   
        }else {
           // generateMyBoard(counter, v.value) 
           makePdf(1, form2)   
        }
     
      counter++;
    }
  })

}
function makePdf(days, form2){
    console.log("makePdf")
    var pdfSets = days;
    var counter = 1;
    // for each day do x amount of sodukos and print to pdf
    for(var i = 0; i <= pdfSets;i++){

    
        $.each(form2, function(k,v){
            console.log(v.name+" :"+v.value)
        
            if (v.name == "diff_level") {
                {
                    var board =  generateMyBoard(counter, v.value, true, i) ;
                  //  var doc = new jsPDF({
                  //      format:"a4"
                  //  })
                    console.log(board)
                    //doc.addSvgAsImage(board, 10, 10,  200,200);
                  // doc.save();
                }
            
            counter++;
            }
        })
      counter = 1;

    }
    

}
function gridData(dataF,dataH, nr2){
 // console.log("griddata")
 // console.log(dataH)
  //console.log(dataF)
 // console.log(dataF.board) // data fuer loesung
 // console.log(dataH)        // data fuer frei
  var counterCol = 0;
  var counterRow = 0
 // var str = dataH.join("\n"); console.log(" my string is : "+str.length)
 // var dataH = str;
  var data = new Array();
    var xpos = 1; //starting xpos and ypos at 1 so the stroke will show when we make the grid below
    var ypos = 1;
    var width = 50;
    var height = 50;
    //var theme = "node";
    // iterate for rows 
    var xml='<?xml version = "1.0" encoding = "UTF-8" ?> \n'
    +'<crossword-compiler xmlns="http://crossword.info/xml/crossword-compiler"> \n'
    +    '<rectangular-puzzle xmlns="http://crossword.info/xml/rectangular-puzzle" alphabet="123456789"> \n'
    +        '<metadata> \n'
    +            '<title>'+ diff +'</title> \n'
    +            '<creator>(c) NTB</creator> \n'
    +            '<copyright></copyright> \n'
    +            '<description></description> \n'
    +        '</metadata> \n'
    +        '<sudoku box-width="3" box-height="3"> \n'
    +            '<grid width="9" height="9"> \n'
    +                '<grid-look thick-border="true" numbering-scheme="none" cell-size-in-pixels="34" clue-square-divider-width="0.7" /> \n';
    var topB;
    var leftB;
    console.log("bgcolor :" + bgcolor)
    for (var row = 0; row < 9; row++) {
        data.push( new Array() );

        // iterate for cells/columns inside rows
        for (var column = 0; column < 9; column++) {
           // console.log("row :" + counterRow)
           // console.log( "col : "+counterCol)
           
           var bgColor = bgcolor2;
           if(theme == "chess" && 
                ((row % 2 == 0 && column % 2 == 0) ||
                row % 2 != 0 && column % 2 != 0)
            ){
               bgColor = bgcolor;     
           }else if (theme == "cross" &&
                ((row != 1 && row != 4 && row != 7) &&
                    (column != 1 && column != 4 && column != 7)
                )
           
           ){
                bgColor = bgcolor; 
           }else if (theme == "cross" &&
                ((row == 1 || row == 4|| row == 7) &&
                    (column == 1 || column == 4 || column == 7)
           )){
                bgColor = bgcolor; 
           }else if(theme == "bigCross" &&
                    ((row != 3 && row != 4 && row != 5) && (
                        column != 3 && column != 4 && column != 5
                    ))){
                        bgColor = bgcolor; 
           }else if(theme == "bigCross" &&
                    ((row == 3 || row == 4 || row == 5) && 
                        (column == 3 || column == 4 || column == 5)
            )){
                        bgColor = bgcolor; 
            } else {
               bgColor = bgcolor2;
           }
           //----     
           // xml
            var hasHint = false;
            if( dataH[counterRow][counterCol] != ""){
                hasHint = true;
            }else {
                hasHint = false;
            }
            if (column === 3 || column === 6){topB = true;}else {topB = false;} // top lines for xml
            if (row === 3 || row === 6){leftB = true;}else {leftB = false;} // side lines for xml
           xml += '<cell x="'+ (row+1) +'" y="'+ (column+1) +'" solution="'+dataF.board[counterRow][counterCol]+'" hint="'+ hasHint+'" top-bar="'+ topB +'"  left-bar="'+ leftB +'" /> \n';
           //const cell = (props) => React.createElement("cell", props);

           //----
         //  console.log( dataH[counterRow][counterCol])
            data[row].push({
                x: xpos,
                y: ypos,
                width: width ,
                height: height,
                value: dataH[counterRow][counterCol], 
                value2: dataF.board[counterRow][counterCol],
                bgColor: bgColor,
                hint:hasHint
            })
            //console.log(" my cell value : " +dataH[counterRow][counterCol])
            // increment the x position. I.e. move it over by 50 (width variable)
            xpos += width;
            counterCol++;
            
        }
        // reset the x position after a row is complete
        xpos = 1;
        // increment the y position for the next row. Move it down 50 (height variable)
        ypos += height; 
        counterRow++;
        counterCol = 0;
       
        
    }
    xml += '</grid> \n</sudoku> \n</rectangular-puzzle> \n</crossword-compiler>';
    // console.log(xml);
    // var parser = new DOMParser();
    xmlDoc = xml;//parser.parseFromString(xml, "text/xml");
    if(nr2 == 1) xml1=xmlDoc;
    else if(nr2 == 2) xml2=xmlDoc;
    else if(nr2 == 3) xml3=xmlDoc;
    else if(nr2 == 4) xml4=xmlDoc;
    else if(nr2 == 5) xml5=xmlDoc;
    else if(nr2 == 6) xml6=xmlDoc;
    else if(nr2 == 7) xml7=xmlDoc;
    return data;
}
var  xmlDoc;
function handleXMLMass(){
    for(var i = 1;i<= sReps; i++){
       var tempXml;
       if(i == 1) tempXml=xml1;
       else if(i == 2) tempXml=xml2;
       else if(i == 3) tempXml=xml3;
       else if(i == 4) tempXml=xml4;
       else if(i == 5) tempXml=xml5;
       else if(i == 6) tempXml=xml6;
       else if(i == 7) tempXml=xml7;
       var blob = new Blob([tempXml], {type:"application/xml"});
       var Filesaver = require('file-saver');
       Filesaver.saveAs(blob, "test"+i+".xml")
    }
    
}
function handleXML(){
   // console.log(handleXML)
   // console.log(xmlDoc)
    //var doc = new DOMParser().parseFromString(xmlDoc);
    var blob = new Blob([xmlDoc], {type:"application/xml"});
    var Filesaver = require('file-saver');
    Filesaver.saveAs(blob, "test.xml")
   // doc.saveAs("sudoku.xml","application/xml");
    
   // console.log(doc);


}
var doc;
function handlePrint(){
    $(".mySpinner").attr("style","display:block;");
  console.log("handleprint")
  //const input  = document.getElementById('myResults');
  const input1 = document.getElementById('side1');
  const input2 = document.getElementById('side2');
  const input3 = document.getElementById('side3');
  const input4 = document.getElementById('side4');
  const input5 = document.getElementById('side5');
  const input6 = document.getElementById('side6');
  const input7 = document.getElementById('side7');

    //var doc = new jsPDF("p","mm");
     doc = new jsPDF({
        format:"a4"
    })
    doc.page = 1;
    var imgWidth = 120; 
    var pageHeight = 295;  
    var d1 = $.Deferred();
    var d2 = $.Deferred();
    var d3 = $.Deferred();
    var d4 = $.Deferred();
    var d5 = $.Deferred();
    var d6 = $.Deferred();
    var d7 = $.Deferred();
    

   // var imgHeight = imgData1.height * imgWidth / imgData1.width;
    //var heightLeft = imgHeight;
    html2canvas(input1,{
        scrollX:0,
        scrollY:0,
        quality:4/*,
        scale:5*/
    })
    .then((canvas1) => {
        makeHeader();
        footer();
        var imgData1 = canvas1.toDataURL('image/png');
        var imgHeight1 = (canvas1.height * imgWidth) / canvas1.width;
       // console.log(canvas1.width)
       // console.log(imgHeight1)
       // console.log(canvas1.height)
       // console.log("image height 1 :" +imgHeight1)
        
        if(imgHeight1 > 0){
            doc.addImage(imgData1,'PNG', 10, 10,  imgWidth,imgHeight1 );
            
            d1.resolve();
        }else {d1.resolve();}
        
        
    });
    if (sReps >1){
    html2canvas(input2,{
            scrollX:0,
            scrollY:0,
            quality:4,
            scale:5
    })
    .then((canvas2) => {
              var imgData2 = canvas2.toDataURL('image/png');
              var imgHeight2 = canvas2.height * imgWidth / canvas2.width;
            //  console.log("image height 2 :" +imgHeight2)
              if(imgHeight2 > 0) {
               
                doc.addPage();
                footer();
                doc.addImage(imgData2,'PNG', 10, 10, imgWidth,imgHeight2 );
                d2.resolve();
            }
            d2.resolve();
    });
}else {
    d2.resolve();
    d3.resolve();
    d4.resolve();
    d5.resolve();
    d6.resolve();
    d7.resolve();
}
    if (sReps > 2){

    
     html2canvas(input3,{
                scrollX:0,
                scrollY:0,
                quality:4/*,
                scale:5'*/
    })
    .then((canvas3) => {
                  var imgData3 = canvas3.toDataURL('image/png');
                  var imgHeight3 = canvas3.height * imgWidth / canvas3.width;
                 // console.log("image height 3 :" +imgHeight3)
                  if(imgHeight3 > 0){
                    doc.addPage();footer();
                       doc.addImage(imgData3,'PNG', 10, 10, imgWidth,imgHeight3 );
                       d3.resolve();
                  }
                  d3.resolve();  
    });
}else {
    d3.resolve();
    d4.resolve();
    d5.resolve();
    d6.resolve();
    d7.resolve();
}
    //make if sReps > 3 ,>4 ... then canvas4 5 ... else d4.resolfe()
    if (sReps > 3){
        html2canvas(input4,{
            scrollX:0,
            scrollY:0,
            quality:4/*,
            scale:5'*/
        })
            .then((canvas4) => {
                    var imgData4 = canvas4.toDataURL('image/png');
                    var imgHeight4 = canvas4.height * imgWidth / canvas4.width;
                    // console.log("image height 3 :" +imgHeight3)
                    if(imgHeight4 > 0){
                        doc.addPage();footer();
                        doc.addImage(imgData4,'PNG', 10, 10, imgWidth,imgHeight4 );
                        d4.resolve();
                    }
                    d4.resolve();  
        });

    }else {
        d4.resolve();
        d5.resolve();
        d6.resolve();
        d7.resolve();
    }
    if (sReps > 4){
        html2canvas(input5,{
            scrollX:0,
            scrollY:0,
            quality:4/*,
            scale:5'*/
        })
            .then((canvas5) => {
                    var imgData5 = canvas5.toDataURL('image/png');
                    var imgHeight5 = canvas5.height * imgWidth / canvas5.width;
                    // console.log("image height 3 :" +imgHeight3)
                    if(imgHeight5 > 0){
                        doc.addPage();footer();
                        doc.addImage(imgData5,'PNG', 10, 10, imgWidth,imgHeight5 );
                        d5.resolve();
                    }
                    d5.resolve();  
        });

    }
    if (sReps > 5){
        html2canvas(input6,{
            scrollX:0,
            scrollY:0,
            quality:4/*,
            scale:5'*/
        })
            .then((canvas6) => {
                    var imgData6 = canvas6.toDataURL('image/png');
                    var imgHeight6 = canvas6.height * imgWidth / canvas6.width;
                    // console.log("image height 3 :" +imgHeight3)
                    if(imgHeight6 > 0){
                        doc.addPage();footer();
                        doc.addImage(imgData6,'PNG', 10, 10, imgWidth,imgHeight6 );
                        d6.resolve();
                    }
                    d6.resolve();  
        });

    }
    if (sReps > 6){
        html2canvas(input7,{
            scrollX:0,
            scrollY:0,
            quality:4/*,
            scale:5'*/
        })
            .then((canvas7) => {
                    var imgData7 = canvas7.toDataURL('image/png');
                    var imgHeight7 = canvas7.height * imgWidth / canvas7.width;
                    // console.log("image height 3 :" +imgHeight3)
                    if(imgHeight7 > 0){
                        doc.addPage();footer();
                        doc.addImage(imgData7,'PNG', 10, 10, imgWidth,imgHeight7 );
                        d7.resolve();
                    }
                    d7.resolve();  
        });

    }
   
    $.when (d1, d2, d3, d4, d5, d6, d7 ).done(function(){
        
        doc.save( 'pdf'+today+'.pdf');
        $(".mySpinner").attr("style","display:none !important;");
        //console.log("saving")
    });
    
   var page =  doc.internal.getNumberOfPages();
   console.log(page + " PAAAAAAAAAAAAAGE")
}
function makeHeader(){
    doc.text(135,10,'generert den ' +dd+'.'+mm+'.'+yyyy);
}
function footer(){
    doc.text(150,285,'side ' +doc.page+ ' av '+ sReps);
    doc.page++;
}
function refreshSudoku(){
    handleSubmit();
}
function App() {
  console.log("APP")
  const [show, setShow] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const ids = ['myResults'];

  var grid = generateMyBoard(1,'0');
  //console.log(grid._groups[0][0])
 // console.log(grid)
  var grid2 = <h2></h2>;
 // generateMyBoard(1,'0');
  return (

    <div className="App">
     
     <div className="print-main page">
     <Button id="refresh-S" title="hente nytt spill" onClick={refreshSudoku} className="btn btn-info" value="Stampa">
            <i className="fa  fa-refresh" aria-hidden="true"></i>
        </Button>
        <Button id="pdf-download" title="pdf print" onClick={handlePrint} className="btn btn-info" value="Stampa">
            <i className="fa  fa-file-pdf-o"></i>
        </Button>
        
        <Button id="xml-download" title="xml print" onClick={handleXML} className="btn btn-info" value="Stampa">
            <i className="fa  fa-file-code-o"></i>
        </Button>
        <Button id="xml-download" title="xml print" onClick={handleXMLMass} className="btn btn-primary" value="Stampa">
            <i className="fa  fa-file-code-o"></i>
        </Button>
        <Button 
          id="btn-settings"
          onClick={() => setOpen(!open) }
          aria-controls="collapseForm"
          title="settings"
          className="btn btn-info"
          aria-expanded={open}>
            <i className="fa fa-cog"></i>
        </Button>

{/*  

        <Button id="settings" type="button" className="btn btn-info" onClick={handleShow}>
            <i className="fa fa-cog"></i>
        </Button>
*/}
    </div>
   {/* <div id="grid4">{grid2}</div>*/}
       <div>
        
          <Collapse in={open} >
            <div id="collapseForm">
              <form id="myForm2" className="form-horizontal" /*onSubmit={handleSubmit}*/>
              
                <div className="form-group">
                   <h6>theme & color</h6> 
                </div>
                <div className="form-group">
                  <label /*for="nod"*/ className="col col-sm-6">theme</label>
                  <div className="col col-sm-7">
                    <select className="form-control" id="theme" name="theme">
                      <option value="none">none</option>
                      <option value="cross">cross</option>
                      <option value="bigCross">bigCross</option>
                      <option value="chess">chess</option>
                      
                    </select>
                  </div>
                </div>
              <div className="form-group">
                    <label  className="col col-sm-6">select font color:  </label>
                  {/*  <input type="color" id="fontcolor" name="fontcolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="fontcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                    <label  className="col col-sm-6">select bg color:     </label>
                    {/* <input type="color" id="bgcolor" name="bgcolor" defaultValue="#fdffff" ></input>*/} 
                    <input type="text" name="bgcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>

                <div className="form-group">
                    <label  className="col col-sm-6">select thickborder color:</label>
                  {/*  <input type="color" id="thickcolor" name="thickcolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="thickcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                    <label  className="col col-sm-6">select thinborder color:</label>
                   {/* <input type="color" id="thincolor" name="thincolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="thincolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >select headline color:</label>
                   {/* <input type="color" id="hlcolor" name="hlcolor" defaultValue="#000000" ></input>*/} 
                    <input type="text" name="hlcolor" className="colorpicker-popup" id="colorpicker-popup" defaultValue="000000"></input>
                </div>
                <div className="form-group">
                   <h6>diff & headlines</h6> 
                </div>
                <div className="form-group">
                  <label /*for="nod"*/ className="col col-sm-6">Number of repeats</label>
                  <div className="col col-sm-7">
                    <select className="form-control" id="nor" name="sudokusReps">
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                     
                      
                    </select>
                  </div>
                </div>

                {/*
              <div className="form-group">
                  <label  className="col col-sm-6">Number of Difficulties</label>
                  <div className="col col-sm-7">
                    <select className="form-control" id="nod" name="sudokus">
                      <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option>
                      
                    </select>
                  </div>
                </div>
                */}
                <div className="form-group">
                  <label /*for="print-dif"*/ className="col col-sm-6">Difficulties to Print</label>
                  <div className="col col-sm-7">
                    <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_0"
                      value="0" />&nbsp;LETT<br />
                    <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_1"
                      value="1" />&nbsp;MIDDELS<br />
                    <input type="checkbox" className="diff_level" name="diff_level" id="diff_level_2"
                      value="2" />&nbsp;VANSKELIG<br />
                    
                  </div>
                </div>
                
                <div className="form-group">
                    <label className="col col-sm-6" >headline text lett</label>
                    <input type="text" id="diff1" name="diff1" defaultValue="LETT" ></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >headline text middels:</label>
                    <input type="text" id="diff2" name="diff2" defaultValue="MIDDELS" ></input>
                </div>
                <div className="form-group">
                    <label className="col col-sm-6" >select headline vanskelig:</label>
                    <input type="text" id="diff3" name="diff3" defaultValue="VANSKELIG" ></input>
                </div>
                

             
                
                <div className="form-group">
                <Button onClick={handleSubmit } onMouseUp={() => setOpen(!open)} >save</Button>
                </div>
               
              </form>
            </div>
          </Collapse>
       </div>
    
    <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
            <Modal.Title>Sudoku Setup</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <div className="row">
                <div className="col-xs-12">
                    <div className="tab-content">
                        <div id="general-settings">
                            <fieldset>
                                <form id="myForm" className="form-horizontal" /*onSubmit={handleSubmit}*/>
                                    <div className="form-group">
                                        <label /*for="nod"*/ className="col col-sm-5">Number of Difficulties</label>
                                        <div className="col col-sm-7">
                                            <select className="form-control" id="nod">
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                                <option value="4">Four</option>
                                                <option value="5">Five</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label /*for="print-dif"*/ className="col col-sm-5">Difficulties to Print</label>
                                        <div className="col col-sm-7">
                                            <input type="checkbox" className="diff_level" id="diff_level_0"
                                                value="0" />&nbsp;ENKLESTE<br />
                                            <input type="checkbox" className="diff_level" id="diff_level_1"
                                                value="0" />&nbsp;LETTI<br />
                                            <input type="checkbox" className="diff_level" id="diff_level_2"
                                                value="0" />&nbsp;MIDDELS<br />
                                            <input type="checkbox" className="diff_level" id="diff_level_3"
                                                value="0" />&nbsp;VANSKELIG<br />
                                            <input type="checkbox" className="diff_level" id="diff_level_4"
                                                value="0" />&nbsp;NÅDELØS<br />
                                        </div>
                                    </div>
                                    {/* 
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Font</label>
                                        <div className="col col-sm-7">
                                            <select className="form-control" id="font-family-boardtitle">
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Font Size</label>
                                        <div className="col col-sm-7">
                                            <div className="input-group">
                                                <input type="text" className="form-control" id="font-size-boardtitle"
                                                    placeholder="Font Size for Board Name" />
                                                <span className="input-group-addon">mm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Color</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="color-boardtitle" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Easiest</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="level-title-0"
                                                placeholder="ENKLESTE" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Easy</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="level-title-1"
                                                placeholder="LETTI" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Medium</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="level-title-2"
                                                placeholder="MIDDELS" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Hard</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="level-title-3"
                                                placeholder="VANSKELIG" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Hardest</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="level-title-4"
                                                placeholder="NÅDELØS" />
                                        </div>
                                    </div>
                           
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Size of Board</label>
                                        <div className="col col-sm-7">
                                            <div className="input-group">
                                                <input type="text" className="form-control" id="sob"
                                                    placeholder="Size of Board" />
                                                <span className="input-group-addon">px</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Font</label>
                                        <div className="col col-sm-7">
                                            <select className="form-control" id="font-family">
                                            </select>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Font Size</label>
                                        <div className="col col-sm-7">
                                            <div className="input-group">
                                                <input type="text" className="form-control" id="font-size"
                                                    placeholder="Font size" />
                                                <span className="input-group-addon">mm</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Color</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="color" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Outer Line</label>
                                        <div className="col col-sm-7">
                                            <div className="input-group">
                                                <input type="text" className="form-control" id="outline-thickness"
                                                    placeholder="Outer line" />
                                                <span className="input-group-addon">px</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Outer Line Color</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="outline-color" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Inner Line</label>
                                        <div className="col col-sm-7">
                                            <div className="input-group">
                                                <input type="text" className="form-control" id="innerline-thickness"
                                                    placeholder="Inner line" />
                                                <span className="input-group-addon">px</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Inner Line Color</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="innerline-color" />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label  className="col col-sm-5">Background Color</label>
                                        <div className="col col-sm-7">
                                            <input type="text" className="form-control" id="bg-color" />
                                        </div>
                                   
                                    
                                    </div> */}
                                </form>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

        </Modal.Body>
        <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
                                    Close
                                    </Button>
                                    <Button variant="primary" onClick={handleSubmit} >
                                        Save Changes
                                    </Button>
                                    <input type="submit" value="Submit" />
        </Modal.Footer>
    </Modal>
</div>

  )
  
}
export default App;
