import React, { Component } from "react";
import pdfFile from "./chart.pdf";

//const pdfFile =
//  "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf";

export default class PDFViewer extends Component {
  handlePrint = () => {
    const node = document.getElementById("myResults");
    node.contentWindow.focus();
    node.contentWindow.print();
  };

  render() {
    return (
      <>
        <h2>PDF Viewer Component</h2>
        <object data={pdfFile} type="application/pdf">
          <embed src={pdfFile} type="application/pdf" />
        </object>
        <button onClick={this.handlePrint}>Print</button>
      </>
    );
  }
}
